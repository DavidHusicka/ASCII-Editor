# Braile ASCII Art Editor

A graphical editor for ASCII art in braille with some effor to be extensible. It allows drawing using a pen tablet and has several brush tools.

## Dependencies

* C++20 compiler
* Qt

## Build instructions

```bash
meson builddir
meson compile -C builddir
```