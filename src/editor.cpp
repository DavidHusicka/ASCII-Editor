#include "shared.hpp"
#include "editor.hpp"
#include <string>
#include <locale>
#include <variant>
#include <codecvt>

const int EMPTY_BRAILLE_CHARACTER = 0x2800; 
const int BLOCK_CHARACTER = 0x2588; // █ character

std::string BrailleCharacter::to_string() const {
    Character character = EMPTY_BRAILLE_CHARACTER + this->character;
    return character.to_string();
}

void BrailleCharacter::draw_braille(uint8_t mask) {
    this->character |= mask;
}

std::string Character::to_string() const {
    // no match statement. i am literally crying
    if (std::holds_alternative<BrailleCharacter>(this->character)) {
        let value = std::get<BrailleCharacter>(this->character);
        return value.to_string();
    } else {
        let value = std::get<uint32_t>(this->character);
        return std::wstring_convert<std::codecvt_utf8<char32_t>, char32_t>().to_bytes(value); // some code from the internet
    }
}

void Character::draw_braille(uint8_t mask) {
    if (std::holds_alternative<BrailleCharacter>(this->character)) {
        auto value = std::get<BrailleCharacter>(this->character);
        value.draw_braille(mask);
    } else {
        this->character = BrailleCharacter(mask);
    }
}

std::string Canvas::to_string() const {
    std::string output;
    for (uint64_t y = 0; y < this->height; y++) {
        for (uint64_t x = 0; x < this->width; x++) {
            output += this->get_character(x, y).to_string();
        }
        output += "\n";
    }
    return output;
}

Character& Canvas::get_character(uint64_t x, uint64_t y) const {
    return this->data[y * this->width + x]; // I hate implicit references
}

void Canvas::draw_braille(uint64_t x, uint64_t y, uint8_t mask) {
    this->get_character(x, y).draw_braille(mask);
}

void Canvas::draw_character(uint64_t x, uint64_t y, uint32_t character) {
    this->get_character(x, y) = character;
}

void BlockBrush::draw(Canvas& canvas, uint64_t x, uint64_t y, uint64_t size) {
    for (uint64_t i = 0; i < size; i++) {
        for (uint64_t j = 0; j < size; j++) {
            if (x + i >= canvas.get_width() || y + j >= canvas.get_height()) {
                continue;
            }
            canvas.draw_character(x + i, y + j, BLOCK_CHARACTER); 
        }
    }
}

void Editor::draw(uint64_t x, uint64_t y, uint64_t size) {
    this->brush->draw(this->canvas, x, y, size);
}

std::string Editor::to_string() const {
    return this->canvas.to_string();
}

Canvas::Canvas(uint64_t width, uint64_t height) : width(width), height(height) {
    this->data = std::make_unique<Character[]>(width * height);
}