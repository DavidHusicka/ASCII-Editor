#include <QCoreApplication>
#include <QDebug>
#include "editor.hpp"

#include <iostream>

int main(int argc, char *argv[])
{
    Editor editor(80, 24);
    editor.draw(5, 5, 2);
    std::cout << editor.to_string() << std::endl;

}