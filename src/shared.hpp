#define let const auto

template<class... Ts> struct overloaded : Ts... { using Ts::operator()...; };