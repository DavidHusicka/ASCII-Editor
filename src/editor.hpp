#pragma once

#include <memory>
#include <variant>

class BrailleCharacter {
    private:
        uint8_t character = 0; // represented as a 2x4 matrix
    public:
        BrailleCharacter() = default;
        BrailleCharacter(uint8_t character) : character(character) {}
        ~BrailleCharacter() = default;

        std::string to_string() const;
        void draw_braille(uint8_t mask);
};

class Character {
    private:
        std::variant<BrailleCharacter, uint32_t> character = 0u;
    public:
        Character() = default;
        Character(uint32_t character) : character(character) {}
        ~Character() = default;
        std::string to_string() const;
        void draw_braille(uint8_t mask);
};

class Canvas {
    private:
        uint64_t width;
        uint64_t height;
        std::unique_ptr<Character[]> data; // a unicode character
    public:
        Canvas(uint64_t width, uint64_t height);
        ~Canvas() = default;

        std::string to_string() const;
        Character& get_character(uint64_t x, uint64_t y) const;
        void draw_braille(uint64_t x, uint64_t y, uint8_t mask);
        void draw_character(uint64_t x, uint64_t y, uint32_t character);
        uint64_t get_width() const { return this->width; }
        uint64_t get_height() const { return this->height; }
};

class Brush {
    private:
    public:
        virtual ~Brush() = default;
        virtual void draw(Canvas& canvas, uint64_t x, uint64_t y, uint64_t size) = 0;
};

class BlockBrush : public Brush {
    private:
    public:
        BlockBrush() = default;
        ~BlockBrush() = default;
        void draw(Canvas& canvas, uint64_t x, uint64_t y, uint64_t size) override;
};

class Editor {
    private:
        uint64_t width;
        uint64_t height;
        Canvas canvas;
        std::unique_ptr<Brush> brush = std::make_unique<BlockBrush>();
    public:
        Editor(uint64_t width, uint64_t height) : width(width), height(height), canvas(width, height) {};
        ~Editor() = default;

        void draw(uint64_t x, uint64_t y, uint64_t size);
        std::string to_string() const;
};